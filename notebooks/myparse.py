import argparse
parser = argparse.ArgumentParser()

parser.add_argument('run_mode',  help='run mode; live, playback, hwil, cifs, testing')

parser.add_argument('-o', '--outut-dir',  default='$HOME/OUTPUT/UNREGISTERED-FLIGHT',
                        dest='output_dir', help='Name of the output folder')

parser.add_argument('-i', '--input-config-yaml',  default='mavan-config.yaml',
                        dest='mavan_config', help='input yaml config file')

parser.add_argument('-f', '--flight-data-directory',  default='\!@#$$12345',
                        dest='playback_dir', help='Flight data')

parser.add_argument('-r', '--run-mavan',  action='store_true',
                        dest='run_mavan', help='Let run-mavan.py to run MaVAN. Otherwise just generate start-mavan.sh file.')

parser.add_argument('-v', '--view-in-terminal',  action='store_true',
                        dest='view_in_terminal', help='View output in an xterm terminal.')

parser.add_argument('-ns', '--no-server',  action='store_true',
                        dest='no_server', help='View output in an xterm terminal.')
results = parser.parse_args()

print(results)
