import sys

# sys.argv is a list of command line arguments
# print(sys.argv)

for idx, item in enumerate(sys.argv):
    print(f"argv[{idx}] = {item}")
